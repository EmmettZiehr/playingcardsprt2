#include <iostream>
#include <conio.h>
#include <string>

// Creators: Nicholas Protogere (Prt.1) & Emmett J. Ziehr (Prt.2).

enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

enum Suit
{
	DIAMOND,
	HEART,
	CLUB,
	SPADE
};

struct Card
{
	Suit cardSuit;
	Rank cardRank;
	std::string suit;
	std::string rank;
};

//Function Prototypes
void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	//Instances of variables.
	Card c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19,
		c20, c21, c22, c23, c24, c25, c26, c27, c28, c29, c30, c31, c32, c33, c34, c35, c36,
		c37, c38, c39, c40, c41, c42, c43, c44, c45, c46, c47, c48, c49, c50, c51, c52;
	//Diamond Cards.
	c1.cardRank = TWO; c1.cardSuit = DIAMOND; c1.suit = "Diamonds"; c1.rank = "Two";
	c2.cardRank = THREE; c2.cardSuit = DIAMOND; c2.suit = "Diamonds"; c2.rank = "Three";
	c3.cardRank = FOUR; c3.cardSuit = DIAMOND; c3.suit = "Diamonds"; c3.rank = "Four";
	c4.cardRank = FIVE; c4.cardSuit = DIAMOND; c4.suit = "Diamonds"; c4.rank = "Five";
	c5.cardRank = SIX; c5.cardSuit = DIAMOND; c5.suit = "Diamonds"; c5.rank = "Six";
	c6.cardRank = SEVEN; c6.cardSuit = DIAMOND; c6.suit = "Diamonds"; c6.rank = "Seven";
	c7.cardRank = EIGHT; c7.cardSuit = DIAMOND; c7.suit = "Diamonds"; c7.rank = "Eight";
	c8.cardRank = NINE; c8.cardSuit = DIAMOND; c8.suit = "Diamonds"; c8.rank = "Nine";
	c9.cardRank = TEN; c9.cardSuit = DIAMOND; c9.suit = "Diamonds"; c9.rank = "Ten";
	c10.cardRank = JACK; c10.cardSuit = DIAMOND; c10.suit = "Diamonds"; c10.rank = "Jack";
	c11.cardRank = QUEEN; c11.cardSuit = DIAMOND; c11.suit = "Diamonds"; c11.rank = "Queen";
	c12.cardRank = KING; c12.cardSuit = DIAMOND; c12.suit = "Diamonds"; c12.rank = "King";
	c13.cardRank = ACE; c13.cardSuit = DIAMOND; c13.suit = "Diamonds"; c13.rank = "Ace";
	//Spade Cards.
	c14.cardRank = TWO; c14.cardSuit = SPADE; c14.suit = "Spades"; c14.rank = "Two";
	c15.cardRank = THREE; c15.cardSuit = SPADE; c15.suit = "Spades"; c15.rank = "Three";
	c16.cardRank = FOUR; c16.cardSuit = SPADE; c16.suit = "Spades"; c16.rank = "Four";
	c17.cardRank = FIVE; c17.cardSuit = SPADE; c17.suit = "Spades"; c17.rank = "Five";
	c18.cardRank = SIX; c18.cardSuit = SPADE; c18.suit = "Spades"; c18.rank = "Six";
	c19.cardRank = SEVEN; c19.cardSuit = SPADE; c19.suit = "Spades"; c19.rank = "Seven";
	c20.cardRank = EIGHT; c20.cardSuit = SPADE; c20.suit = "Spades"; c20.rank = "Eight";
	c21.cardRank = NINE; c21.cardSuit = SPADE; c21.suit = "Spades"; c21.rank = "Nine";
	c22.cardRank = TEN; c22.cardSuit = SPADE; c22.suit = "Spades"; c22.rank = "Ten";
	c23.cardRank = JACK; c23.cardSuit = SPADE; c23.suit = "Spades"; c23.rank = "Jack";
	c24.cardRank = QUEEN; c24.cardSuit = SPADE; c24.suit = "Spades"; c24.rank = "Queen";
	c25.cardRank = KING; c25.cardSuit = SPADE; c25.suit = "Spades"; c25.rank = "King";
	c26.cardRank = ACE; c26.cardSuit = SPADE; c26.suit = "Spades"; c26.rank = "Ace";
	//Heart Cards.
	c27.cardRank = TWO; c27.cardSuit = HEART; c27.suit = "Hearts"; c27.rank = "Two";
	c28.cardRank = THREE; c28.cardSuit = HEART; c28.suit = "Hearts"; c28.rank = "Three";
	c29.cardRank = FOUR; c29.cardSuit = HEART; c29.suit = "Hearts"; c29.rank = "Four";
	c30.cardRank = FIVE; c30.cardSuit = HEART; c30.suit = "Hearts"; c30.rank = "Five";
	c31.cardRank = SIX; c31.cardSuit = HEART; c31.suit = "Hearts"; c31.rank = "Six";
	c32.cardRank = SEVEN; c32.cardSuit = HEART; c32.suit = "Hearts"; c32.rank = "Seven";
	c33.cardRank = EIGHT; c33.cardSuit = HEART; c33.suit = "Hearts"; c33.rank = "Eight";
	c34.cardRank = NINE; c34.cardSuit = HEART; c34.suit = "Hearts"; c34.rank = "Nine";
	c35.cardRank = TEN; c35.cardSuit = HEART; c35.suit = "Hearts"; c35.rank = "Ten";
	c36.cardRank = JACK; c36.cardSuit = HEART; c36.suit = "Hearts"; c36.rank = "Jack";
	c37.cardRank = QUEEN; c37.cardSuit = HEART; c37.suit = "Hearts"; c37.rank = "Queen";
	c38.cardRank = KING; c38.cardSuit = HEART; c38.suit = "Hearts"; c38.rank = "King";
	c39.cardRank = ACE; c39.cardSuit = HEART; c39.suit = "Hearts"; c39.rank = "Ace";
	//Club Cards.
	c40.cardRank = TWO; c40.cardSuit = CLUB; c40.suit = "Clubs"; c40.rank = "Two";
	c41.cardRank = THREE; c41.cardSuit = CLUB; c41.suit = "Clubs"; c41.rank = "Three";
	c42.cardRank = FOUR; c42.cardSuit = CLUB; c42.suit = "Clubs"; c42.rank = "Four";
	c43.cardRank = FIVE; c43.cardSuit = CLUB; c43.suit = "Clubs"; c43.rank = "Five";
	c44.cardRank = SIX; c44.cardSuit = CLUB; c44.suit = "Clubs"; c44.rank = "Six";
	c45.cardRank = SEVEN; c45.cardSuit = CLUB; c45.suit = "Clubs"; c45.rank = "Seven";
	c46.cardRank = EIGHT; c46.cardSuit = CLUB; c46.suit = "Clubs"; c46.rank = "Eight";
	c47.cardRank = NINE; c47.cardSuit = CLUB; c47.suit = "Clubs"; c47.rank = "Nine";
	c48.cardRank = TEN; c48.cardSuit = CLUB; c48.suit = "Clubs"; c48.rank = "Ten";
	c49.cardRank = JACK; c49.cardSuit = CLUB; c49.suit = "Clubs"; c49.rank = "Jack";
	c50.cardRank = QUEEN; c50.cardSuit = CLUB; c50.suit = "Clubs"; c50.rank = "Queen";
	c51.cardRank = KING; c51.cardSuit = CLUB; c51.suit = "Clubs"; c51.rank = "King";
	c52.cardRank = ACE; c52.cardSuit = CLUB; c52.suit = "Clubs"; c52.rank = "Ace";

	PrintCard(c1);
	HighCard(c1, c2);

	_getch();
	return 0;
}

void PrintCard(Card card)
{
	std::cout << "The " << card.rank << " of " << card.suit << std::endl;
}

Card HighCard(Card card1, Card card2)
{
	if (card1.cardRank > card2.cardRank) { return card1; }
	else { return card2; }
}